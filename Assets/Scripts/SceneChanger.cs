﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class SceneChanger : MonoBehaviour
{
    public string levelName;

    public bool isBlue = false;
    public bool isRed = false;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player_BottomTrigger"))
        {
            string parentTag = collision.transform.parent.tag;
            if (parentTag == "Player_Blue")
            {
                isBlue = true;
            }
            if (parentTag == "Player_Red")
            {
                isRed = true;
            }
        }

        if(isBlue == true && isRed == true)
        {
            SceneManager.LoadScene(levelName);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        isBlue = false;
        isRed = false;
    }
}
