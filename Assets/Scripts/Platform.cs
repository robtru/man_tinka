﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platform : MonoBehaviour
{

    public Sprite blue;
    public Sprite red;

    public LayerMask blueMask;
    public LayerMask redMask;

    public bool isStandable = true;
    public bool isColored = false;

    public bool makeBlue;
    public bool makeRed;

    SpriteRenderer sr;
    PlatformEffector2D effector;

    private Sprite startSprite;
    private LayerMask startMask;

    // Start is called before the first frame update
    void Start()
    {
        sr = GetComponent<SpriteRenderer>();
        effector = GetComponent<PlatformEffector2D>();

        startSprite = sr.sprite;
        if(effector != null)
        {
            startMask = effector.colliderMask;
        }
        if (makeBlue)
        {
            MakeBlue();
        }
        if (makeRed)
        {
            MakeRed();
        }

    }

    public void MakeBlue()
    {
        sr.sprite = blue;
        gameObject.layer = LayerMask.NameToLayer("Blue");
        if(effector != null)
        {
            effector.colliderMask = blueMask;
        }
    }

    public void MakeRed()
    {
        sr.sprite = red;
        gameObject.layer = LayerMask.NameToLayer("Red");
        if(effector != null)
        {
            effector.colliderMask = redMask;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (isStandable)
        {
            if (collision.CompareTag("Player_BottomTrigger") && !isColored)
            {
                string parentTag = collision.transform.parent.tag;
                if (parentTag == "Player_Blue")
                {
                    isColored = true;
                    MakeBlue();
                }
                else if (parentTag == "Player_Red")
                {
                    isColored = true;
                    MakeRed();
                }
            }
        }
        else
        {
            if (collision.CompareTag("Player_Blue") && !isColored)
            {
                isColored = true;
                MakeBlue();
            }
            else if (collision.CompareTag("Player_Red") && !isColored)
            {
                isColored = true;
                MakeRed();
            }
        }
    }
}
